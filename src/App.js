//App.js
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';

import './App.css';
import { UserProvider } from './UserContext';


function App() {

    const [user, setUser] = useState({ id : null, isAdmin : null });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {

        if(localStorage.getItem('token')){
            fetch("http://localhost:4000/users/details", {
                headers : {
                    Authorization : `Bearer ${ localStorage.getItem('token') }`
                }
            })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                // if(data._id){
                    setUser({
                        id: data._id,
                        isAdmin : data.isAdmin
                    })
                // }
            })
        };

        console.log(user);
        console.log(localStorage);
    }, []);

    return (

    <UserProvider value={{ user, setUser, unsetUser }}>

        <Router>      
            <AppNavbar />
            <Container>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/courses" element={<Courses />} />
                    <Route path="/courses/:courseId" element={<CourseView />} />                   
                    <Route path="/login" element={<Login />} />
                    <Route path="/logout" element={<Logout />} /> 
                    <Route path="/register" element={<Register />} />      
                    <Route path="/*" element={<Error />} />     
                </Routes>
            </Container>
        </Router>
    </UserProvider>

      
  );
}

export default App;
